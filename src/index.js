import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import SessionContextProvider from './Contexts/Session/SessionContext';



ReactDOM.render(
  <React.StrictMode>
    
    <SessionContextProvider>
        <App />
    </SessionContextProvider>
    
  </React.StrictMode>,
  document.getElementById('root')
);

serviceWorker.unregister();
