import React, { useContext } from 'react';
import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom';
import './App.css';
import LoginPage from './Pages/LoginPage/LoginPage.jsx';
import SignupPage from './Pages/SignupPage/SignupPage.jsx';
import HomePage from './Pages/HomePage/HomePage.jsx';
import LogoutPage from './Pages/LogoutPage/LogoutPage.jsx';
import WallPage from './Pages/WallPage/WallPage.jsx';
import { SessionContext } from './Contexts/Session/SessionContext';
import { ModalContext } from './Contexts/Modal/ModalContext';
import ModalCreate from './Components/ModalCreate/ModalCreate.jsx';
import { motion, AnimatePresence } from 'framer-motion';
import ModalContextProvider from './Contexts/Modal/ModalContext';
import SignupContextProvider from './Contexts/Signup/SignupContext';
import LoginContextProvider from './Contexts/Login/LoginContext';


function App() {
 const { session } = useContext(SessionContext);
//  const { modal } = useContext(ModalContext);
  // const { modalState } = useContext(ModalContext);
  // console.log(loginSessions._token);
  // console.log(modalState._modalOpen);
  // const { dispatch } = useContext(LoginSessionsContext);
  // let timer;

  // const handleLogout = () => {
  //   timer = setTimeout(() => {
  //     console.log("logout and destroy token");
  //     dispatch({ type: 'LOGOUT', values: { 
  //       _errorCode: 0, 
  //       _userId: loginSessions._userId
  //     }});
      
  //   }, 900000);
  // }

  // const handleAuth = () => {
  //   if (loginSessions._token) {
  //   clearTimeout(timer);
  //   handleLogout();   
  //   } 
  // }

  const pageVariants = {
    in: { opacity: 1 },
    out: { opacity: 0 },
  }
  
  const pageTransitions = {
    duration: 1,
    ease: "easeInOut"
  }

  return (
  
    <div className="app" >
      
    <LoginContextProvider> <SignupContextProvider> <ModalContextProvider>
      <BrowserRouter>
        <Switch>             
            <Route path="/login" component={LoginPage} />
            <Route path="/signup" component={SignupPage} />
            <Route path="/logout" component={LogoutPage} />       
            <Route path="/wall" component={WallPage} />             
            <Route path="/" component={HomePage} />
        </Switch>
        { session.token && ( <Redirect from="/login" to="/wall" exact /> ) }    
      </BrowserRouter>
      </ModalContextProvider> </SignupContextProvider> </LoginContextProvider>
    </div>
    
  );
}

export default App;


