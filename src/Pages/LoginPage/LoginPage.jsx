import  React, { useContext } from 'react';
import './LoginPage.css';
import Navbar from '../../Components/Navbar/Navbar';
import Login from '../../Components/Login/Login';
import ModalCreate from '../../Components/ModalCreate/ModalCreate';
import { ModalContext } from '../../Contexts/Modal/ModalContext';


function LoginPage() {
  const { modal } = useContext(ModalContext);


return (
<>
  <div className="grid-container-login">
      <div className="navbar-login"> <Navbar/> </div>
        <div className="login"> <Login/> </div>
      <div className="footer-login"> </div>
  </div>

</>
  );
}


export default LoginPage;
