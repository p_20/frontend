import  React, {  } from 'react';
import './SignupPage.css';
import Navbar from '../../Components/Navbar/Navbar';
import Signup from '../../Components/Signup/Signup';
import SignupContextProvider from '../../Contexts/Signup/SignupContext';


function SignupPage() {


return (
<>

  <div className="grid-container-signup">
    <div className="navbar-signup"> <Navbar/> </div> 
      <div className="signup"> <Signup/> </div>  
      <div className="footer-signup"> </div>
  </div>

</>
);
}


export default SignupPage;
