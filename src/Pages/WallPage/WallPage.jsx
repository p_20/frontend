import  React, { useContext } from 'react';
import './WallPage.css';
import Navbar from '../../Components/Navbar/Navbar';
import NoteGroup from '../../Components/NoteGroup/NoteGroup';
import { motion, AnimatePresence } from 'framer-motion';
import ModalCreate from '../../Components/ModalCreate/ModalCreate';
import { ModalContext } from '../../Contexts/Modal/ModalContext';


function WallPage() {
  const { modal } = useContext(ModalContext);

  const variants = {
    in: { opacity: 1 },
    out: { opacity: 0 },
  }

  const transitions = {
    duration: 1,
    ease: "easeInOut"
  }
  

return (
<>

  <div className="grid-container-wall"> 
    <div className="navbar-wall"> <Navbar/> </div> 
      <motion.div initial="out" animate="in" exit="out" variants={variants} transition={transitions} className="wall"> <NoteGroup/> </motion.div> 
  </div>

</> 

  );
}


export default WallPage;



