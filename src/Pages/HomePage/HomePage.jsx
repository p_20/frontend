import  React, { useContext } from 'react';
import './HomePage.css';
import Navbar from '../../Components/Navbar/Navbar';
import { motion, AnimatePresence } from 'framer-motion';
import ModalCreate from '../../Components/ModalCreate/ModalCreate';
import ModalModify from '../../Components/ModalModify/ModalModify';
import { ModalContext } from '../../Contexts/Modal/ModalContext';

function HomePage() {
  const { modal } = useContext(ModalContext);

  const variants = {
    in: {
      opacity: 1
          
    },
    out: {
      opacity: 0
      
    },

  }

  const transitions = {
    duration: 1,
    ease: "easeInOut"
  }
  

return (
<>
{/* <div> { modal.modalOpen === true && <Modal/> } */}
  <div className="grid-container-home">
    <div className="navbar-home"> <Navbar/> </div>
      {/* <motion.div initial="out" animate="in" exit="out" variants={variants} transition={transitions} className="home"> Welcome </motion.div>  */}
      {/* <div className="home"> <ModalModify/> </div> */}
      <div className="footer-home"> </div>
  </div>
{/* </div> */}
</> 
);
}


export default HomePage;



