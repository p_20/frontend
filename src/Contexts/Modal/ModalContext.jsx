import React, { createContext, useReducer, useEffect } from 'react';
import { modalReducer } from './ModalReducer';


export const ModalContext = createContext();

function ModalContextProvider(props) {

  const initial = {
    modalCreateOpen: false,
    isHovered: { field: null, bool: false },
    isValidationError: { errorCode: null, bool: false },
    incorrectField: { header: false, body: false },
    noteModify: { modalModifyOpen: null, _id: null, header: null, body: null, userFk: null, creationDate: null }
  }


  const [modal, modalDispatch] = useReducer(modalReducer, initial, () => {
    const localData = localStorage.getItem('modal');
    return localData ? JSON.parse(localData) : initial;
  });

  useEffect(() => {
    localStorage.setItem('modal', JSON.stringify(modal));
  }, [modal]);

  return (
    <ModalContext.Provider value={{modal, modalDispatch}}>
        {props.children}        
    </ModalContext.Provider>
  );
}

export default ModalContextProvider;

