export const modalReducer = (state, action) => {
    switch (action.type) { 
        case 'RESET':
            return {
                modalOpen: false,
                isSuccesfull: null,
                isHovered: { 
                    field: null, 
                    bool: null 
                },
                isValidationError: { 
                    errorCode: null, 
                    bool: null
                },
                incorrectField: { 
                    login: null, 
                    password: null, 
                    repassword: null, 
                    email: null
                }
            }
        case 'SET_MODAL_CREATE':
            return {
                ...state,
                modalCreateOpen: action.values.modalCreateOpen
            }  
        case 'SET_NOTE_MODIFY':
            return {
                ...state,
                noteModify: { 
                    _id: action.values._id, 
                    header: action.values.header, 
                    body: action.values.body, 
                    userFk: action.values.userFk, 
                    creationDate: action.values.creationDate,
                    updateDate: action.values.updateDate,
                    modalModifyOpen: action.values.modalModifyOpen
                }
            }                 
        case 'SET_IS_HOVERED':
                return {
                    ...state,
                    isHovered: { 
                        field: action.values.isHoveredField, 
                        bool: action.values.isHoveredBool 
                    }
                }  
        case 'SET_IS_VALIDATION_ERROR':
                return {
                    ...state,
                    isValidationError: { 
                        errorCode: action.values.isValidationErrorErrorCode, 
                        bool: action.values.isValidationErrorBool 
                    }
                }    
        case 'SET_INCORRECT_FIELD':
                return {
                    ...state,
                    incorrectField: { 
                        header: action.values.incorrectFieldHeader, 
                        body: action.values.incorrectFieldBody,     
                    }
                }                       
        default:
            return {
                ...state
            };    
    }
}

