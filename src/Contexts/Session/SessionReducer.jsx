export const sessionReducer = (state, action) => {
    switch (action.type) { 
        case 'RESET':
            return {
                isLog: null,
                userId: null,
                token: null,
                tokenRefresh: null,
                errorCode: null
            }
        case 'LOGIN':
            return {
                isLog: true,
                userId: action.values.userId,
                token: action.values.token,
                tokenRefresh: action.values.tokenRefresh,
                errorCode: action.values.errorCode
            }
        case 'LOGOUT': 
            return {
                isLog: false,
                userId: action.values.userId,
                token: "",
                tokenRefresh: "",
                errorCode: null
            }  
        case 'FAILED_LOGIN': 
            return {
                isLog: false,
                userId: null,
                token: "",
                tokenRefresh: "",
                errorCode: action.values.errorCode
            } 
        case 'TOKEN_REFRESH':      
            return {
                isLog: true,
                userId: action.values.userId,
                token: action.values.token,
                tokenRefresh: action.values.tokenRefresh,
                errorCode: action.values.errorCode
            }
        default:
            return {
                ...state
            };    
    }
}

