import React, { createContext, useReducer, useEffect } from 'react';
import { sessionReducer } from './SessionReducer';


export const SessionContext = createContext();

function SessionContextProvider(props) {

  const initial = {
    isLog: null,
    userId: null,
    token: null,
    tokenRefresh: null,
    errorCode: null
  }

  const [session, sessionDispatch] = useReducer(sessionReducer, initial, () => {
    const localData = localStorage.getItem('session');
    return localData ? JSON.parse(localData) : initial;
  });

  useEffect(() => {
    localStorage.setItem('session', JSON.stringify(session));
  }, [session]);

  return (
    <SessionContext.Provider value={{session, sessionDispatch}}>
        {props.children}        
    </SessionContext.Provider>
  );
}

export default SessionContextProvider;

