import React, { createContext, useReducer, useEffect } from 'react';
import { signupReducer } from './SignupReducer';


export const SignupContext = createContext();

function SignupContextProvider(props) {

  const initial = {
    isSuccesfull: false, 
    isHovered: { field: null, bool: false },
    isValidationError: { errorCode: null, bool: false },
    incorrectField: { login: false, password: false, repassword: false, email: false }
  }

  const [signup, signupDispatch] = useReducer(signupReducer, initial
    , () => {
    const localData = localStorage.getItem('signup');
    return localData ? JSON.parse(localData) : initial;
  }
  );

  useEffect(() => {
    localStorage.setItem('signup', JSON.stringify(signup));
  }, [signup]);

  return (
    <SignupContext.Provider value={{signup, signupDispatch}}>
      {props.children}        
    </SignupContext.Provider>
  );
}

export default SignupContextProvider;
