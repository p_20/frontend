export const signupReducer = (state, action) => {
    switch (action.type) { 
        case 'RESET':
            return {
                isSuccesfull: null,
                isHovered: { 
                    field: null, 
                    bool: null 
                },
                isValidationError: { 
                    errorCode: null, 
                    bool: null
                },
                incorrectField: { 
                    login: null, 
                    password: null, 
                    repassword: null, 
                    email: null
                }
            }
        case 'SET_IS_SUCCESFULL':
                return {
                    ...state,
                    isSuccesfull: action.values.isSuccesfull
                }    
        case 'SET_IS_HOVERED':
                return {
                    ...state,
                    isHovered: { 
                        field: action.values.isHoveredField, 
                        bool: action.values.isHoveredBool 
                    }
                }  
        case 'SET_IS_VALIDATION_ERROR':
                return {
                    ...state,
                    isValidationError: { 
                        errorCode: action.values.isValidationErrorErrorCode, 
                        bool: action.values.isValidationErrorBool 
                    }
                }    
        case 'SET_INCORRECT_FIELD':
                return {
                    ...state,
                    incorrectField: { 
                        login: action.values.incorrectFieldLogin, 
                        password: action.values.incorrectFieldPassword, 
                        repassword: action.values.incorrectFieldRepassword, 
                        email: action.values.incorrectFieldEmail
                    }
                }                          
        default:
            return {
                ...state
            };    
    }
}

