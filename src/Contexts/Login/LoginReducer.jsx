export const loginReducer = (state, action) => {
    switch (action.type) { 
        case 'RESET':
            return {
                incorrectField: { 
                    login: null, 
                    password: null
                }
            } 
        case 'SET_INCORRECT_FIELD':
            return {
                incorrectField: { 
                    login: action.values.incorrectFieldLogin,
                    password: action.values.incorrectFieldPassword
                }
            }                                            
        default:
            return {
                ...state
            };    
    }
}

