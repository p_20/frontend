import React, { createContext, useReducer, useEffect } from 'react';
import { loginReducer } from './LoginReducer';


export const LoginContext = createContext();

function LoginContextProvider(props) {

  const initial = {
    incorrectField: { login: false, password: false }
  }

  const [login, loginDispatch] = useReducer(loginReducer, initial
    , () => {
    const localData = localStorage.getItem('login');
    return localData ? JSON.parse(localData) : initial;
  }
  );

  useEffect(() => {
    localStorage.setItem('login', JSON.stringify(login));
  }, [login]);

  return (
    <LoginContext.Provider value={{login, loginDispatch}}>
      {props.children}        
    </LoginContext.Provider>
  );
}

export default LoginContextProvider;
