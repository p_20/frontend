import  React, { useState, useEffect, useContext } from 'react';
import './NoteGroup.css';
import { SessionContext } from '../../Contexts/Session/SessionContext';
import Note from '../Note/Note';
import { ModalContext } from '../../Contexts/Modal/ModalContext';
import { motion, AnimatePresence } from 'framer-motion';


function NoteGroup() {
  const { sessionDispatch } = useContext(SessionContext);
  const { session } = useContext(SessionContext);
  const { modal } = useContext(ModalContext);

  const [notes, setNotes] = useState({
    values: [],
    errorCode: null
  });

  async function handleReqest() {

    const request = {
      query: `
          query {
            notes {
              _id
              header
              title
              body
              footer
              userFk
              creationDate
              updateDate
              authOutput {                   
                token
                tokenRefresh
                userId
                errorCode
              } 
            }
          }
        `,
        variables: {

        }
    };
    
    let response = await fetch(`http://${process.env.REACT_APP_IP}:${process.env.REACT_APP_PORT_DOCKER}/graphql`, {
      method: 'POST',
      body: JSON.stringify(request),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + session.token,
        'AuthorizationRefresh': 'Bearer ' + session.tokenRefresh
      } 
    });

    let data;
    if (response.status !== 200 && response.status !== 201) {
    throw new Error('Failed!');    
    }
    else {
    data = await response.json();     
    }    
    return data;
}

async function fetchNotes(event) {
  // event.preventDefault();
  handleReqest()
  .then(resData => {
    const { data } = resData;
    const { notes } = data;

  //  console.log(notes);
    

    if (notes.length > 0 && notes[0].authOutput.errorCode === 0) {
      if ( notes[0].authOutput.errorCode === 0 && notes[0].authOutput.token !== session.token ) {
        sessionDispatch({ type: 'TOKEN_REFRESH', values: { 
          errorCode: notes[0].authOutput.errorCode, 
          userId: notes[0].authOutput.userId,
          token: notes[0].authOutput.token,
          tokenRefresh: notes[0].authOutput.tokenRefresh
        }
        })
      }

      setNotes({values: notes, errorCode: notes[0].authOutput.errorCode});

      
    } 
    

  })
  .catch(error => {
    console.log(error);
    setNotes({errorCode: -3});
  });
}

useEffect(() => {
  let mounted = true;
  console.log('mounting start')
  if(mounted){
    fetchNotes();
  }

  return () => { console.log('unmounting...'); mounted = false }

}, [modal.noteModify.modalModifyOpen, modal.modalCreateOpen]);

const variantsButton = {
  onHowerButton: { scale: 0.95 },
  onTapButton: { scale: 1.0 },
}



return (
<>
  <div className="container-note-group">

      {notes.values.map((note) => <motion.div whileHover="onHowerButton" whileTap="onTapButton" variants={variantsButton} key={note._id}> <Note note={note} editMode={false} />  </motion.div>)}

  </div>
</>
);
}


export default NoteGroup;


