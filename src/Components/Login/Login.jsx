import  React, { useState, useRef, useContext } from 'react';
import './Login.css';
import { SessionContext } from '../../Contexts/Session/SessionContext';
import { LoginContext } from '../../Contexts/Login/LoginContext';
import TextField from '../TextField/TextField';
import { NavLink } from 'react-router-dom';
import { motion, AnimatePresence } from 'framer-motion';


function Login() {
  const { loginDispatch } = useContext(LoginContext);
  const { login } = useContext(LoginContext);
  const { sessionDispatch } = useContext(SessionContext);
  const { session } = useContext(SessionContext);
  let loginRef = useRef(null);
  let passwordRef = useRef(null);

  async function handleFetch() {
        const login = loginRef.current.value;
        const password = passwordRef.current.value;

        const request = {
          query: `
            query Login($login: String!, $password: String!) {
              login(login: $login, password: $password) {
                userId
                token
                tokenRefresh
                errorCode
              }
            }
          `,
          variables: {
            login: login,
            password: password
          }
        }

        let response = await fetch(`http://${process.env.REACT_APP_IP}:${process.env.REACT_APP_PORT_DOCKER}/graphql`, {
              method: 'POST',
              body: JSON.stringify(request),
              headers: {
                'Content-Type': 'application/json'
              } 
            });

      let data;
      if (response.status !== 200 && response.status !== 201) {
        throw new Error('Failed!');    
      }
      else {
        data = await response.json();         
      }    

      return data;
}   


async function handleLogin(event) {

    event.preventDefault();
    handleFetch()
      .then(resData => {
        if (resData.data.login.errorCode === -2) {
          loginDispatch({ type: 'SET_INCORRECT_FIELD', values: { incorrectFieldPassword: true, incorrectFieldLogin: false }}); 
          sessionDispatch({ type: 'FAILED_LOGIN', values: { 
            errorCode: resData.data.login.errorCode, 
            userId: resData.data.login.userId,
            token: resData.data.login.token,
            tokenRefresh: resData.data.login.tokenRefresh
          }});    
        }
        else if (resData.data.login.errorCode === -1) {
          loginDispatch({ type: 'SET_INCORRECT_FIELD', values: { incorrectFieldPassword: false, incorrectFieldLogin: true }});
          sessionDispatch({ type: 'FAILED_LOGIN', values: { 
            errorCode: resData.data.login.errorCode, 
            userId: resData.data.login.userId,
            token: resData.data.login.token,
            tokenRefresh: resData.data.login.tokenRefresh
          }});
        }
        else if (resData.data.login.errorCode === -3) {
          loginDispatch({ type: 'SET_INCORRECT_FIELD', values: { incorrectFieldPassword: true, incorrectFieldLogin: true }});
          sessionDispatch({ type: 'FAILED_LOGIN', values: { 
            errorCode: resData.data.login.errorCode, 
            userId: resData.data.login.userId,
            token: resData.data.login.token,
            tokenRefresh: resData.data.login.tokenRefresh
          }});
        }
        else if ((resData.data.login.errorCode === 0)) {
          loginDispatch({ type: 'SET_INCORRECT_FIELD', values: { incorrectFieldPassword: false, incorrectFieldLogin: false }});
          sessionDispatch({ type: 'LOGIN', values: { 
            errorCode: resData.data.login.errorCode, 
            userId: resData.data.login.userId,
            token: resData.data.login.token,
            tokenRefresh: resData.data.login.tokenRefresh
          }});  
        }
      })
      .catch(error => {
        sessionDispatch({ type: 'FAILED_LOGIN', values: { 
          errorCode: -3
         } });
      });
  }

  const variantsButton = {
    onHowerButton: { scale: 1.2 },
    onTapButton: { scale: 0.8 },
  }

  const variantsMessage = {
    in: { scale: 1.25, opacity: 1 },
    hidden: { scale: 0, opacity: 0 },
  }

  const transitionsMessage = {
    duration: 1,
    ease: "easeInOut"
  }

  async function resetLogin() {
    sessionDispatch({ type: 'RESET', values: { }});   
  }

return (
  <>
     <div className="login-form" >
      
      <div className="l1">
        <TextField  forwardedRef={loginRef} fieldName={"login"} inputType={"text"} incorrect={login.incorrectField.login}/>
      </div>
    
      <div className="l2">
        <TextField  forwardedRef={passwordRef} fieldName={"password"} inputType={"password"} incorrect={login.incorrectField.password}/>
      </div>

      <motion.div whileHover="onHowerButton" whileTap="onTapButton" variants={variantsButton} className="button-login-container" onClick={handleLogin}>
        <div className="button-login"> 
          <div className="text-login"> Login </div> 
        </div>
      </motion.div>

      <AnimatePresence>
      { session.errorCode < 0 && <motion.div initial="hidden" animate="in" exit="hidden" variants={variantsMessage} transition={transitionsMessage} className="bottom-text-login-container">
          <div className="bottom-text-login">
            { session.errorCode === -1 && <motion.p onClick={resetLogin}> User does not exit! </motion.p> }
            { session.errorCode === -2 && <motion.p onClick={resetLogin}> Incorrect password! </motion.p> }
            { session.errorCode === -3 && <motion.p onClick={resetLogin}> Try later! </motion.p> }
          </div>   
        </motion.div> }   
      </AnimatePresence>
    </div> 
</>
  );
}


export default Login;
