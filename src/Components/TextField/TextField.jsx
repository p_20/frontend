import  React, { useState, useContext } from 'react';
import './TextField.css';



function TextField({fieldName, inputType, incorrect, forwardedRef}) {





  const [fieldCssStyle, setStyle] = useState({
    lineStyle: "line_change_1",
    descriptionStyle: "description_change_1"
  });

  const handleMouseEnter = () => {
    if (forwardedRef.current.value.length === 0 ) {
      setStyle({ lineStyle: "line_change_2", descriptionStyle: "description_change_2" });
    }
  }

  const handleMouseLeave = () => {
    if (forwardedRef.current.value.length === 0 ) {
      setStyle({ lineStyle: "line_change_1", descriptionStyle: "description_change_1" });
    }
  }

  const handleTextInput = () => {
    if (forwardedRef.current.value.length > 0 ) {
      setStyle({ lineStyle: "line_change_2", descriptionStyle: "description_change_2" });
    }
    else {
      setStyle({ lineStyle: "line_change_1", descriptionStyle: "description_change_1" });
    }
  }

return (
  <>

      <div className="field" onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave} onChange={handleTextInput} >
          <input type={inputType} ref={forwardedRef}/>
          <div id="description" className={fieldCssStyle.descriptionStyle}> {fieldName} </div>
          <div id="line" className={fieldCssStyle.lineStyle}></div>
          {incorrect ? ( <div id="line" className="line_change_3"></div> ) : ( <div id="line" className={fieldCssStyle.lineStyle}></div> )}
      </div>

</>
  );
}


export default TextField;

