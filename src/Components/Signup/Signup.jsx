import  React, { useState, useRef, useContext, useEffect } from 'react';
import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom';
import './Signup.css';
import TextField from '../TextField/TextField';
import { motion, AnimatePresence } from 'framer-motion';
import { SignupContext } from '../../Contexts/Signup/SignupContext';

function Signup() {
  const { signupDispatch } = useContext(SignupContext);
  const { signup } = useContext(SignupContext);
  const loginRef = useRef(null);
  const passwordRef = useRef(null);
  const emailRef = useRef(null);
  const repasswordRef = useRef(null);
    
  async function handleFetch() {
    const email = emailRef.current.value;
    const repassword = repasswordRef.current.value;
    const login = loginRef.current.value;
    const password = passwordRef.current.value;

    const request = {
      query: `
        mutation CreateUser($login: String!, $password: String!, $repassword: String!, $email: String!) {
          createUser(login: $login, password: $password, repassword: $repassword, email: $email) {
            userId
            errorCode
          }
      }`,
        variables: {
          login: login,
          password: password,
          repassword: repassword,
          email: email
        }
      }

    let response = await fetch(`http://${process.env.REACT_APP_IP}:${process.env.REACT_APP_PORT_DOCKER}/graphql`, {
          method: 'POST',
          body: JSON.stringify(request),
          headers: {
            'Content-Type': 'application/json'
          } 
        });

  let data;
  if (response.status !== 200 && response.status !== 201) {
    throw new Error('Failed!');    
  }
  else {
    data = await response.json();         
  }    

    return data;
  }   

  async function handleSignup(event) {

    event.preventDefault();
    handleFetch()
    .then(resData => {

      if (resData.data.createUser.errorCode < 0) {
        signupDispatch({ type: 'SET_IS_VALIDATION_ERROR', values: { isValidationErrorErrorCode: resData.data.createUser.errorCode, isValidationErrorBool: true }});
        if (resData.data.createUser.errorCode === -4) {
          signupDispatch({ type: 'SET_INCORRECT_FIELD', values: { incorrectFieldLogin: true }});
        }
        else if (resData.data.createUser.errorCode === -8) {
          signupDispatch({ type: 'SET_INCORRECT_FIELD', values: { incorrectFieldLogin: true }});
        }
        else if (resData.data.createUser.errorCode === -5) {
          signupDispatch({ type: 'SET_INCORRECT_FIELD', values: { incorrectFieldEmail: true }});
        }
        else if (resData.data.createUser.errorCode === -6) {  
          signupDispatch({ type: 'SET_INCORRECT_FIELD', values: { incorrectFieldPassword: true }});
        }
        else if (resData.data.createUser.errorCode === -7) {
          signupDispatch({ type: 'SET_INCORRECT_FIELD', values: { incorrectFieldRepassword: true }});
        }
      }
      else if ((resData.data.createUser.errorCode === 0)) {
          signupDispatch({ type: 'SET_IS_VALIDATION_ERROR', values: { isValidationErrorErrorCode: resData.data.createUser.errorCode, isValidationErrorBool: false }}) 
          let timer = setTimeout(() => { signupDispatch({ type: 'SET_IS_SUCCESFULL', values: { isSuccesfull: true }})  }, 1000);
      }    
    })
    .catch(error => {
      signupDispatch({ type: 'SET_IS_VALIDATION_ERROR', values: { isValidationErrorErrorCode: -3, isValidationErrorBool: true }});
    });
  }



  const variantsButton = {
    onHowerButton: { scale: 1.2 },
    onTapButton: { scale: 0.8 },
  }

  const variantsMessage = {
    in: { scale: 1.25, opacity: 1 },
    hidden: { scale: 0, opacity: 0 },
  }

  const transitionsMessage = {
    duration: 1,
    ease: "easeInOut"
  }

  async function setField(value) {
  let timer = setTimeout(() => {     
    signupDispatch({ type: 'SET_IS_HOVERED', values: { isHoveredField: value, isHoveredBool: true }})}, 1000);
  }

  async function resetField() {
    signupDispatch({ type: 'SET_IS_HOVERED', values: { 
      isHoveredField: null,
      isHoveredBool: false
     }
    });  
  }

return (
  <>
        {/* <AnimatePresence> */}
    {signup.isSuccesfull === true && <Redirect from="/signup" to="/" exact />}

    <div className="signup-form">

      <div className="s1" onMouseEnter={() => setField("email")} onMouseLeave={resetField} >
        <TextField  forwardedRef={emailRef} fieldName={"email"} inputType={"text"} incorrect={signup.incorrectField.email}/>
      </div>

      <div className="s2" onMouseEnter={() => setField("login")} onMouseLeave={resetField} >
        <TextField  forwardedRef={loginRef} fieldName={"login"} inputType={"text"} incorrect={signup.incorrectField.login}/>
      </div>

      <div className="s3" onMouseEnter={() => setField("password")} onMouseLeave={resetField} >
        <TextField forwardedRef={passwordRef} fieldName={"password"} inputType={"password"} incorrect={signup.incorrectField.password}/>
      </div>

      <div className="s4" onMouseEnter={() => setField("repassword")} onMouseLeave={resetField} >
        <TextField forwardedRef={repasswordRef} fieldName={"password"} inputType={"password"} incorrect={signup.incorrectField.repassword}/>
      </div>

      <motion.div whileHover="onHowerButton" whileTap="onTapButton" variants={variantsButton} className="button-signup-container" onClick={handleSignup}>
        <div className="button-signup"> 
          <div className="text-signup"> Sign up </div> 
        </div>
      </motion.div>

      <AnimatePresence>
        { signup.isValidationError.bool !== true && signup.isHovered.bool === true && <motion.div initial="hidden" animate="in" exit="hidden" variants={variantsMessage} transition={transitionsMessage} className="bottom-text-signup-container">
          <div className="bottom-text-signup">
            { signup.isHovered.field === "login" && <motion.p> Login should contain at least 8 alphanumeric characters, <br></br> without any special characters. </motion.p> }
            { signup.isHovered.field === "password" && <motion.p> Password should contain at least 8 alphanumeric characters, <br></br> with 3 special characters. </motion.p> }
            { signup.isHovered.field === "email" && <motion.p> Email should be provided in proper way. </motion.p> }
            { signup.isHovered.field === "repassword" && <motion.p> Password should be the same. </motion.p> }
          </div>   
        </motion.div> }   

        { signup.isValidationError.bool === true && <motion.div initial="hidden" animate="in" exit="hidden" variants={variantsMessage} transition={transitionsMessage} className="bottom-text-signup-container">
          <div className="bottom-text-signup">
            { signup.isValidationError.errorCode === -8 && <motion.p> Login is too short. </motion.p> }
            { signup.isValidationError.errorCode === -4 && <motion.p> User already exist. </motion.p> }
            { signup.isValidationError.errorCode === -5 && <motion.p> Email is incorrect. </motion.p> }
            { signup.isValidationError.errorCode === -6 && <motion.p> Password is incorrect. </motion.p> }
            { signup.isValidationError.errorCode === -7 && <motion.p> Password is not the same. </motion.p> }
            { signup.isValidationError.errorCode === -3 && <motion.p> Try later. </motion.p> }
          </div>   
        </motion.div> }   

        { signup.isValidationError.bool !== true && signup.isValidationError.errorCode === 0 && <motion.div initial="hidden" animate="in" exit="hidden" variants={variantsMessage} transition={transitionsMessage} className="bottom-text-signup-container">
          <div className="bottom-text-signup">
            <motion.p> User created. </motion.p> 
          </div>   
        </motion.div> }  
         
      </AnimatePresence>
    </div>
    {/* </AnimatePresence> */}
</>
  );
}


export default Signup;

