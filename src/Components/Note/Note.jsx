import  React, { useContext } from 'react';
import './Note.css';
import { ModalContext } from '../../Contexts/Modal/ModalContext';


function Note(state) {
  const { modalDispatch } = useContext(ModalContext);
  const { modal } = useContext(ModalContext);
  let noteDateTime = null;


  if (state.note.updateDate !== null) {
    noteDateTime = new Date(state.note.updateDate);
  }
  else {
    noteDateTime = new Date(state.note.creationDate);
  }

  const currentDateTime = new Date();
  const diffTime = Math.abs( currentDateTime - noteDateTime );
  const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)) - 1; 



  async function selectNote(event) {
    modalDispatch({ type: 'SET_NOTE_MODIFY', values: { 
      _id: state.note._id, 
      header: state.note.header, 
      body: state.note.body, 
      userFk: state.note.userFk, 
      creationDate: state.note.creationDate,
      updateDate: state.note.updateDate,
      modalModifyOpen: true
    }})

  }

return (
<>
    { state.editMode !== true && 
      <div className="note" onClick={selectNote}>
        <div className="note-header"> {state.note.header} </div>
        <div className="note-body"> {state.note.body} </div>
        <div className="note-footer"> {diffDays} days ago </div>
      </div>
    }

    { state.editMode === true && 
      <div className="note">
        <div className="note-header"> <input ref={state.forwardedHeaderRef} placeholder={state.note.header}></input> </div>
        <div className="note-body"> <input ref={state.forwardedBodyRef} placeholder={state.note.body}></input> </div>
        <div className="note-footer"> {diffDays} days ago </div> 
      </div>  
    } 

</>
);
}


export default Note;


