import  React, { useState, useContext } from 'react';
import { NavLink } from 'react-router-dom';
import './Navbar.css';
import { SessionContext } from '../../Contexts/Session/SessionContext';
import { ModalContext } from '../../Contexts/Modal/ModalContext';
import { motion, AnimatePresence } from 'framer-motion';
import ModalCreate from '../ModalCreate/ModalCreate.jsx';
import ModalModify from '../ModalModify/ModalModify.jsx';



function Navbar() {
  const { session } = useContext(SessionContext);
  const { sessionDispatch } = useContext(SessionContext);
  const { modalDispatch } = useContext(ModalContext);
  const { modal } = useContext(ModalContext);

  const [buttonCssStyle, setButtonStyle] = useState({
    buttonStyle: "buttonUnClick"
  });

  const buttonAction = () => {
    if (buttonCssStyle.buttonStyle === "buttonUnClick") {
      setButtonStyle({ buttonStyle: "buttonClick" });
    }
    else if (buttonCssStyle.buttonStyle === "buttonClick") {
      setButtonStyle({ buttonStyle: "buttonUnClick" });
    }
  }

  async function resetModal(event) {
    modalDispatch({ type: 'SET_MODAL_CREATE', values: { modalCreateOpen: false }})

    modalDispatch({ type: 'SET_NOTE_MODIFY', values: { 
        _id: null, 
        header: null, 
        body: null, 
        userFk: null, 
        creationDate: null
      }
    })
  }

  async function handleModalCreate(event) {
    modalDispatch({ type: 'SET_MODAL_CREATE', values: { modalCreateOpen: true }})
    buttonAction();
  }

  async function handleLogout(event) {
    sessionDispatch({ type: 'LOGOUT', values: { 
      errorCode: 0, 
      userId: session.userId
    }});  

    modalDispatch({ type: 'SET_MODAL_CREATE', values: { modalCreateOpen: false }})

    modalDispatch({ type: 'SET_NOTE_MODIFY', values: { 
        _id: null, 
        header: null, 
        body: null, 
        userFk: null, 
        creationDate: null
      }
    })

  }

  const variants = {
    onHower: {
      scale: 1.2         
    },
    onTap: {
      scale: 0.8      
    },
  }

  const pageVariants = {
    in: { opacity: 1 },
    out: { opacity: 0 },
  }
  
  const pageTransitions = {
    duration: 2,
    ease: "easeInOut"
  }

return (
   
    <div>

      {/* <AnimatePresence>
        <motion.div initial="out" animate="in" exit="out" variants={pageVariants} transition={pageTransitions} > */}
          { modal.modalCreateOpen === true && <ModalCreate/> }
          { modal.noteModify.modalModifyOpen === true && <ModalModify/> }
        {/* </motion.div>
      </AnimatePresence> */}


      <nav>
        <label className="checkbtn">
          <motion.i whileHover="onHower" whileTap="onTap" variants={variants} className="fas fa-bars" onClick={buttonAction}></motion.i>
        </label>

        <label className="logo"> P20 </label>

        <ul className={buttonCssStyle.buttonStyle}>
          
          <motion.li onClick={resetModal} whileHover="onHower" whileTap="onTap" variants={variants}><NavLink to="/home"> Home </NavLink></motion.li>

          { session.isLog === true && <motion.li whileHover="onHower" whileTap="onTap" variants={variants} onClick={handleModalCreate}><a> Create Note </a></motion.li> }    
          
          { session.isLog === true && <motion.li onClick={resetModal} whileHover="onHower" whileTap="onTap" variants={variants}><NavLink to="/wall"> Wall </NavLink></motion.li> }           

          { session.isLog !== true && <motion.li onClick={resetModal} whileHover="onHower" whileTap="onTap" variants={variants}><NavLink to="/login"> Login </NavLink></motion.li> }  

          { session.isLog !== true && <motion.li onClick={resetModal} whileHover="onHower" whileTap="onTap" variants={variants}><NavLink to="/signup"> Sign up </NavLink></motion.li> }  

          { session.isLog === true && <motion.li whileHover="onHower" whileTap="onTap" variants={variants} onClick={handleLogout}><NavLink to="/logout"> Logout </NavLink></motion.li> }   
 
        </ul>

      </nav>

    </div>

  );
}


export default Navbar;



