import  React, { useState, useRef, useContext } from 'react';
import './ModalModify.css';
import { SessionContext } from '../../Contexts/Session/SessionContext';
import { ModalContext } from '../../Contexts/Modal/ModalContext';
import Note from '../Note/Note';
import TextField from '../TextField/TextField';
import { motion, AnimatePresence } from 'framer-motion';

function ModalModify() {
  const { modalDispatch } = useContext(ModalContext);
  const { modal } = useContext(ModalContext);
  const { sessionDispatch } = useContext(SessionContext);
  const { session } = useContext(SessionContext);
  let headerRef = useRef(null);
  let bodyRef = useRef(null);


  async function handleReqestDelete() {
    const _id = modal.noteModify._id;

    const request = {
      query: `
      mutation deleteNote($_id: ID!) {
        deleteNote(_id: $_id) {
              _id
              header
              body
              userFk
              creationDate
              updateDate
              authOutput {                   
                token
                tokenRefresh
                userId
                errorCode
              } 
            }
          }
        `,
        variables: {
            _id: _id
        }
    };

    let response = await fetch('http://localhost:9000/graphql', {
      method: 'POST',
      body: JSON.stringify(request),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + session.token,
        'AuthorizationRefresh': 'Bearer ' + session.tokenRefresh
      } 
    });

    let data;
    if (response.status !== 200 && response.status !== 201) {
    throw new Error('Failed!');    
    }
    else {
    data = await response.json();         
    }    
    return data;
}




async function handleReqestUpdate() {
    const userFk = session.userId; 
    const header = headerRef.current.value;
    const body = bodyRef.current.value;
    const _id = modal.noteModify._id;

    const request = {
      query: `
      mutation updateNote($_id: ID!, $header: String!, $body: String!) {
          updateNote(_id: $_id, header: $header, body: $body) {
              _id
              header
              body
              userFk
              creationDate
              updateDate
              authOutput {                   
                token
                tokenRefresh
                userId
                errorCode
              } 
            }
          }
        `,
        variables: {
            _id: _id,
            header: header,
            body: body
        }
    };



    let response = await fetch(`http://${process.env.REACT_APP_IP}:${process.env.REACT_APP_PORT_DOCKER}/graphql`, {
      method: 'POST',
      body: JSON.stringify(request),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + session.token,
        'AuthorizationRefresh': 'Bearer ' + session.tokenRefresh
      } 
    });

    let data;
    if (response.status !== 200 && response.status !== 201) {
    throw new Error('Failed!');    
    }
    else {
    data = await response.json();         
    }    
    return data;
}

async function modifyNote(event) {
  event.preventDefault();
  handleReqestUpdate()
  .then(resData => {
    console.log(resData);
    const { data } = resData;
    const { updateNote } = data;

    if ( resData.data.updateNote.authOutput.errorCode === 0 && resData.data.updateNote.authOutput.token !== session._token ) {
      sessionDispatch({ type: 'TOKEN_REFRESH', values: { 
        errorCode: resData.data.updateNote.authOutput.errorCode, 
        userId: resData.data.updateNote.authOutput.userId,
        token: resData.data.updateNote.authOutput.token,
        tokenRefresh: resData.data.updateNote.authOutput.tokenRefresh
        }
      });           
    }

    if ( resData.data.updateNote.authOutput.errorCode === 0 ) {
    modalDispatch({ type: 'SET_NOTE_MODIFY', values: { 
        modalModifyOpen: false,
        _id: null,
        header: null,
        body: null,
        userFk: null,
        creationDate: null,
        updateDate: null
      }
    })
  }

  })
  .catch(error => {
    console.log(error);
  });
}



async function handleCancel(event) {
  modalDispatch({ type: 'SET_NOTE_MODIFY', values: { 
    _id: null, 
    header: null, 
    body: null, 
    userFk: null, 
    creationDate: null,
    updateDate: null, 
    modalModifyOpen: false
  }})
}

async function deleteNote(event) {
  event.preventDefault();
  handleReqestDelete()
  .then(resData => {
    console.log(resData);
    const { data } = resData;
    const { deleteNote } = data;

    if ( resData.data.deleteNote.authOutput.errorCode === 0 && resData.data.deleteNote.authOutput.token !== session._token ) {
      sessionDispatch({ type: 'TOKEN_REFRESH', values: { 
        errorCode: resData.data.deleteNote.authOutput.errorCode, 
        userId: resData.data.deleteNote.authOutput.userId,
        token: resData.data.deleteNote.authOutput.token,
        tokenRefresh: resData.data.deleteNote.authOutput.tokenRefresh
        }
      });           
    }

    if ( resData.data.deleteNote.authOutput.errorCode === 0 ) {
    modalDispatch({ type: 'SET_NOTE_MODIFY', values: { 
        modalModifyOpen: false,
        _id: null,
        header: null,
        body: null,
        userFk: null,
        creationDate: null,
        updateDate: null
      }
    })
  }

  })
  .catch(error => {
    console.log(error);
  });
}

const variantsModalModify = {
  in: { scale: 1.25, opacity: 1 },
  hidden: { scale: 0, opacity: 0 },
}

const transitionsModalModify = {
  duration: 0.5,
  ease: "easeInOut"
}

const variantsButton = {
  onHowerButton: { scale: 1.2 },
  onTapButton: { scale: 0.8 },
}

return (
<>
  <div className="modal-modify-background">

          <div className="modal-modify-container">

              <motion.div className="modify-area" initial="hidden" animate="in" exit="hidden" variants={variantsModalModify} transition={transitionsModalModify}  >
                  <Note note={modal.noteModify} editMode={true} forwardedHeaderRef={headerRef} forwardedBodyRef={bodyRef} /> 
              </motion.div> 

              <motion.div className="buttons-modal-modify-container" initial="hidden" animate="in" exit="hidden" variants={variantsModalModify} transition={transitionsModalModify} >
                  <motion.div whileHover="onHowerButton" whileTap="onTapButton" variants={variantsButton} onClick={modifyNote} >
                    <div className="button-modify"  >
                      <div className="text-create"> Modify </div> 
                    </div>
                  </motion.div>

                  <motion.div whileHover="onHowerButton" whileTap="onTapButton" variants={variantsButton} onClick={deleteNote} >
                    <div className="button-delete"  >
                      <div className="text-create"> Delete </div> 
                    </div>
                  </motion.div>

                  <motion.div whileHover="onHowerButton" whileTap="onTapButton" variants={variantsButton} onClick={handleCancel} >
                    <div className="button-cancel"  >
                      <div className="text-create"> Cancel </div> 
                    </div>
                  </motion.div>
              </motion.div>

          </div>
  </div> 
</>
);
}


export default ModalModify;


