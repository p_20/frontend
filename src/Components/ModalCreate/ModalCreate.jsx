import  React, { useState, useRef, useContext } from 'react';
import './ModalCreate.css';
import { SessionContext } from '../../Contexts/Session/SessionContext';
import { ModalContext } from '../../Contexts/Modal/ModalContext';
import TextField from '../TextField/TextField';
import { motion, AnimatePresence } from 'framer-motion';

function ModalCreate() {
  const { modalDispatch } = useContext(ModalContext);
  const { modal } = useContext(ModalContext);
  const { sessionDispatch } = useContext(SessionContext);
  const { session } = useContext(SessionContext);
  let headerRef = useRef(null);
  let bodyRef = useRef(null);


async function handleReqest() {
    const header = headerRef.current.value;
    const body = bodyRef.current.value;
    const userFk = session.userId; 

    const request = {
      query: `
          mutation CreateNote($header: String!, $body: String!, $userFk: ID!) {
            createNote(header: $header, body: $body, userFk: $userFk) {
              _id
              header
              body
              userFk
              authOutput {                   
                token
                tokenRefresh
                userId
                errorCode
              } 
            }
          }
        `,
        variables: {
            header: header,
            body: body,
            userFk: userFk
        }
    };
    
    let response = await fetch(`http://${process.env.REACT_APP_IP}:${process.env.REACT_APP_PORT_DOCKER}/graphql`, {
      method: 'POST',
      body: JSON.stringify(request),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + session.token,
        'AuthorizationRefresh': 'Bearer ' + session.tokenRefresh
      } 
    });

    let data;
    if (response.status !== 200 && response.status !== 201) {
    throw new Error('Failed!');    
    }
    else {
    data = await response.json();         
    }    
    return data;
}

async function createNote(event) {
  event.preventDefault();
  handleReqest()
  .then(resData => {
    if (resData.data.createNote.authOutput.errorCode < 0) {
      modalDispatch({ type: 'SET_IS_VALIDATION_ERROR', values: { isValidationErrorErrorCode: resData.data.createNote.authOutput.errorCode, isValidationErrorBool: true }}) 
      if (resData.data.createNote.authOutput.errorCode === -10) {
        modalDispatch({ type: 'SET_INCORRECT_FIELD', values: { incorrectFieldHeader: true, incorrectFieldBody: false }});
      }
      else if (resData.data.createNote.authOutput.errorCode === -11) {
        modalDispatch({ type: 'SET_INCORRECT_FIELD', values: { incorrectFieldHeader: false, incorrectFieldBody: true }});
      }
      }
    else if (resData.data.createNote.authOutput.errorCode === 0) {
      let timer = setTimeout(() => { modalDispatch({ type: 'SET_MODAL_CREATE', values: { modalCreateOpen: false }}) }, 1000);
    }  
    else if ( resData.data.createNote.authOutput.errorCode === 0 && resData.data.createNote.authOutput.token !== session._token ) {
      sessionDispatch({ type: 'TOKEN_REFRESH', values: { 
        errorCode: resData.data.createNote.authOutput.errorCode, 
        userId: resData.data.createNote.authOutput.userId,
        token: resData.data.createNote.authOutput.token,
        tokenRefresh: resData.data.createNote.authOutput.tokenRefresh
        }
      });           
    }
    }
  )
  .catch(error => {
    console.log(error);
    modalDispatch({ type: 'SET_IS_VALIDATION_ERROR', values: { isValidationErrorErrorCode: -3, isValidationErrorBool: true }}) 
  });
}

async function handleCancel(event) {
  let timer = setTimeout(() => { modalDispatch({ type: 'SET_MODAL_CREATE', values: { modalCreateOpen: false }}) }, 250);
}

const variantsButton = {
  onHowerButton: { scale: 1.2 },
  onTapButton: { scale: 0.8 },
}

const variantsModalCreate = {
  in: { scale: 1, opacity: 1 },
  hidden: { scale: 0, opacity: 0 },
}

const transitionsModalCreate = {
  duration: 0.5,
  ease: "easeInOut"
}

const variantsMessage = {
  in: { scale: 1.25, opacity: 1 },
  hidden: { scale: 0, opacity: 0 },
}

const transitionsMessage = {
  duration: 1,
  ease: "easeInOut"
}

return (
<>
  <div className="modal-create-background">
      <motion.div className="modal-create-form" initial="hidden" animate="in" exit="hidden" variants={variantsModalCreate} transition={transitionsModalCreate}>

        <div className="m1-create">
          <div className="m1-create-text"> Create New Note </div>      
        </div>

        <div className="m2-create">
          <TextField  forwardedRef={headerRef} fieldName={"Header"} inputType={"text"} incorrect={modal.incorrectField.header}/>
        </div>

        <div className="m3-create">
          <TextField  forwardedRef={bodyRef} fieldName={"Body"} inputType={"text"} incorrect={modal.incorrectField.body}/>
        </div>

        <div className="buttons-modal-create-container">
          <motion.div whileHover="onHowerButton" whileTap="onTapButton" variants={variantsButton} onClick={createNote} >
            <div className="button-create"  >
              <div className="text-create"> Create </div> 
            </div>
          </motion.div>

          <motion.div whileHover="onHowerButton" whileTap="onTapButton" variants={variantsButton} onClick={handleCancel} >
            <div className="button-cancel-2"  >
              <div className="text-create"> Cancel </div> 
            </div>
          </motion.div>
        </div>

        <AnimatePresence>
        { modal.isValidationError.bool === true && <motion.div initial="hidden" animate="in" exit="hidden" variants={variantsMessage} transition={transitionsMessage} className="bottom-text-modal-create-container">
            <div className="bottom-text-modal-create">
              { modal.isValidationError.errorCode === -10 && <motion.p> Header is too short. </motion.p> }
              { modal.isValidationError.errorCode === -11 && <motion.p> Body is too short. </motion.p> }
              { modal.isValidationError.errorCode === -3 && <motion.p> Try later. </motion.p> }
            </div>   
          </motion.div> }    
        </AnimatePresence>
      </motion.div>
  </div> 
</>
);
}


export default ModalCreate;


